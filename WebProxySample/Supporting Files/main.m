//
//  main.m
//  WebProxySample
//
//  Created by Nicolas on 2016-01-15.
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
