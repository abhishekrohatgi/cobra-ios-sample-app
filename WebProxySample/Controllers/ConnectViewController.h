//
//  ConnectViewController.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "WebAPIConnection.h"

@interface ConnectViewController : UIViewController

@property (strong, nonatomic) MBProgressHUD *hud;
- (void) reset;

@end
