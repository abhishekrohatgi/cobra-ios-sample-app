//
//  SecondViewController.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "TTSViewController.h"

@interface TTSViewController ()
@property(strong, nonatomic) AVAudioPlayer * audioPlayer;
@property (weak, nonatomic) IBOutlet UIButton *ttsButton;
@end

@implementation TTSViewController


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"TTSViewController::viewDidLoad");
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"TTSViewController::didReceiveMemoryWarning");
    // Dispose of any resources that can be recreated.
}


#pragma mark - IBActions
- (IBAction)ttsButtonPressed:(UIButton *)sender {
    NSLog(@"TTSViewController::ttsButtonPressed");
    if (!self.textView.text || self.textView.text.length == 0) {
        return;
    }
    [self dismissKeyboard];
    self.ttsButton.enabled = false;
    self.ttsButton.alpha = 0.5;
    
    WebAPITTSCommand* ttsCommand = [[WebAPITTSCommand alloc] init:self.textView.text];
    ttsCommand.delegate = self;
    [ttsCommand send];
    
    self.textView.text = @"Processing...";
    self.textView.userInteractionEnabled = false;
}


#pragma mark - WebAPITTSCommandDelegate

-(void) webAPITTSCommandDidReceiveAudio:(NSData *)data {
    NSLog(@"TTSViewController::webAPITTSCommandDidReceiveAudio");
    NSError* error;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithData:data error:&error];
    if (error) {
        [self webAPITTSCommandDidFail:[NSString stringWithFormat:@"TTSViewControllerError in initializing audio player - %@", error]];
        return;
    }
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
}

-(void) webAPITTSCommandDidFinish:(NSString *)text {
    self.ttsButton.enabled = true;
    self.ttsButton.alpha = 1.0;
    self.textView.text = @"";
    self.textView.userInteractionEnabled = true;
}

-(void) webAPITTSCommandDidFail:(NSString *)text {
    NSLog(@"TTSViewController::webAPITTSCommandDidFail");
    self.ttsButton.enabled = true;
    self.ttsButton.alpha = 1.0;
    self.textView.text = text;
    self.textView.userInteractionEnabled = true;
}


@end
