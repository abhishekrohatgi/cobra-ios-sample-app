//
//  TabBarController.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectViewController.h"

@interface TabBarController : UITabBarController <WebAPIConnectionDelegate, WebAPISessionDelegate>

@end
