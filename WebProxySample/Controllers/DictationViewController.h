//
//  FirstViewController.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "UIColor+MyColors.h"
#import "TabViewController.h"
#import "WebAPIMRECCommand.h"

@interface DictationViewController : TabViewController <WebAPIMRECCommandDelegate>


@end

