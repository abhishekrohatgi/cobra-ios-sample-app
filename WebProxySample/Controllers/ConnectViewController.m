//
//  ConnectViewController.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "ConnectViewController.h"

@interface ConnectViewController ()

@property (weak, nonatomic) IBOutlet UIButton *connectButton;

@end

@implementation ConnectViewController

- (MBProgressHUD *) hud {
    if (!_hud) _hud = [[MBProgressHUD alloc] init];
    return _hud;
}


#pragma mark - Action

- (IBAction)connectButtonPressed:(UIButton *)sender {
    NSLog(@"ConnectViewController::connectButtonPressed");
    
    self.connectButton.enabled = false;
    self.connectButton.alpha = 0.5;
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:true];
    self.hud.labelText = @"Connecting...";
    
    NSURL* url = [[NSURL alloc] initWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"websocket"]];
    [[WebAPIConnection sharedConnection] connect:url];
}

#pragma mark - Public Function

-(void) reset {
    if (![self.hud isHidden]){
        [self.hud hide:true];
    }
    self.connectButton.enabled = true;
    self.connectButton.alpha = 1.0;
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"ConnectViewController::viewDidLoad");
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"ConnectViewController::didReceiveMemoryWarning");
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"ConnectViewController::prepareForSegue");
}

@end
