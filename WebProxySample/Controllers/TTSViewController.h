//
//  SecondViewController.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "WebAPITTSCommand.h"
#import "TabViewController.h"

@interface TTSViewController : TabViewController <WebAPITTSCommandDelegate>


@end

