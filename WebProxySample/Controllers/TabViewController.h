//
//  TabViewController.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebAPIConnection.h"

@interface TabViewController : UIViewController <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;
-(void) dismissKeyboard;
-(void) keyboardWillChangeFrame: (NSNotification*) notification;

@end
