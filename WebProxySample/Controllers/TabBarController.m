//
//  TabBarController.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()
@property(weak, nonatomic) ConnectViewController* connectVC;
@end

@implementation TabBarController


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"TabBarController::viewDidLoad");
    [WebAPIConnection sharedConnection].connectionDelegate = self;
    [WebAPIConnection sharedConnection].sessionDelegate = self;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"TabBarController::viewDidAppear");
    [self showConnectScreen];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"TabBarController::viewDidLoad");
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private functions

-(void) showConnectScreen {
    if (self.connectVC) {
        [self.connectVC reset];
    } else {
        [self performSegueWithIdentifier:@"ShowConnectScreen" sender:self];
    }
}

-(void) hideConnectScreen {
    if (self.connectVC) {
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

-(void) showAlert: (NSString*)title Message:(NSString*)message {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alert addAction:okButton];
    [self.connectVC presentViewController:alert animated:YES completion:nil];
}


#pragma mark - WebAPIConnectionDelegate
    
-(void) webAPIConnectionDidConnect {
    NSLog(@"TabBarController::webAPIConnectionDidConnect");
    self.connectVC.hud.labelText = @"Starting Session...";
    [[WebAPIConnection sharedConnection] startSession];
}

-(void) webAPIConnectionDidFail:(NSError *)error {
    NSLog(@"TabBarController::webAPIConnectionDidFail");
    [self.connectVC reset];
    [self showConnectScreen];
    [self showAlert:@"Error" Message:error.localizedDescription];
}

-(void) webAPIConnectionDidDisconnect:(NSString *)reason {
    NSLog(@"TabBarController::webAPIConnectionDidDisconnect");
    [self showConnectScreen];
    //[self showAlert:@"Disconnected" Message:reason];
}


#pragma mark - WebAPISessionDelegate

-(void) webAPISessionDidStartSession:(NSString *)text {
    NSLog(@"TabBarController::webAPISessionDidStartSession");
    [self hideConnectScreen];
}

-(void) webAPISessionDidFail:(NSString *)text {
    NSLog(@"TabBarController::webAPISessionDidFail");
    [self showConnectScreen];
    [self showAlert:@"Error" Message:text];
}

-(void) webAPISessionDidEnd:(NSString *)text {
    NSLog(@"TabBarController::webAPISessionDidEnd");
    [self showConnectScreen];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"TabBarController::prepareForSegue");
    ConnectViewController * vc = segue.destinationViewController;
    if (vc) {
        self.connectVC = vc;
    }
}

@end
