//
//  FirstViewController.m
//  WebProxySample
//
//  Copyright 2016 Nuance. All rights reserved.
//

#import "DictationViewController.h"

@interface DictationViewController ()
@property (strong, nonatomic) WebAPIMRECCommand * mrecCommand;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

@end

@implementation DictationViewController


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"DictationViewController::viewDidLoad");
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"DictationViewController::didReceiveMemoryWarning");
    // Dispose of any resources that can be recreated.
}



#pragma mark - Private Functions

-(void) stopRecording: (BOOL) endOfSpeech {
    NSLog(@"DictationViewController::stopRecording");
    if (!endOfSpeech) {
        [self.mrecCommand stopRecording];
    }
    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    self.recordButton.backgroundColor = [UIColor aquaColor];
    self.recordButton.enabled = false;
    self.recordButton.alpha = 0.5;
}


#pragma mark - IBActions

- (IBAction)recordButtonPressed:(UIButton *)sender {
    NSLog(@"DictationViewController::recordButtonPressed");
    if (!self.mrecCommand) {
        self.mrecCommand = [[WebAPIMRECCommand alloc] init];
        self.mrecCommand.delegate = self;
    }
    self.textView.text = @"";
    
    if ([self.mrecCommand isRecording]) {
        [self stopRecording:false];
    } else {
        [self.recordButton setTitle:@"Stop Recording" forState:UIControlStateNormal];
        self.recordButton.backgroundColor = [UIColor flatRedColor];
        [self.mrecCommand startRecording];
    }
}


#pragma mark - WebAPIMRECCommandDelegate

-(void)webAPIMRECCommandDidReceiveStartOfSpeech:(NSString *)text {
    NSLog(@"DictationViewController::webAPIMRECCommandDidReceiveStartOfSpeech");
}

-(void) webAPIMRECCommandDidReceiveEndOfSpeech:(NSString *)text {
    NSLog(@"DictationViewController::webAPIMRECCommandDidReceiveEndOfSpeech");
    [self stopRecording:true];
}

-(void) webAPIMRECCommandDidReceiveIntermediateResult:(NSString *)response {
    NSLog(@"DictationViewController::webAPIMRECCommandDidReceiveIntermediateResult");
    self.textView.text = response;
}

-(void) webAPIMRECCommandDidSucceed:(NSString *)response {
    NSLog(@"DictationViewController::webAPIMRECCommandDidSucceed");
    self.mrecCommand = nil;
    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    self.recordButton.backgroundColor = [UIColor aquaColor];
    self.recordButton.enabled = true;
    self.recordButton.alpha = 1.0;
    self.textView.text = response;
}

-(void) webAPIMRECCommandDidFail:(NSString *)text {
    NSLog(@"DictationViewController::webAPIMRECCommandDidFail");
    self.mrecCommand = nil;
    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    self.recordButton.backgroundColor = [UIColor aquaColor];
    self.recordButton.enabled = true;
    self.recordButton.alpha = 1.0;
    self.textView.text = text;
}

@end
