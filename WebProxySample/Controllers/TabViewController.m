//
//  TabViewController.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "TabViewController.h"

@interface TabViewController ()
@property (strong, nonatomic) UITapGestureRecognizer * tapGR;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@end

@implementation TabViewController


#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"TabViewController::viewDidLoad");
    self.tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"TabViewController::viewWillAppear");
    self.textView.delegate = self;
    self.textView.text = @"";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    NSLog(@"TabViewController::viewWillDisappear");
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    NSLog(@"TabViewController::didReceiveMemoryWarning");
    // Dispose of any resources that can be recreated.
}


#pragma mark - Public functions

-(void) dismissKeyboard {
    NSLog(@"TabViewController::keyboardWillChangeFrame");
    [self.view endEditing:true];
}

-(void) keyboardWillChangeFrame:(NSNotification *)notification {
    if (self.bottomConstraint) {
        NSDictionary * userInfo = notification.userInfo;
        
        // Get information about the animation.
        NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        // Get information about the frame.
        CGRect keyboardEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        // Update the bottom constraint.
        self.bottomConstraint.constant = (keyboardEndFrame.origin.y < self.view.frame.size.height) ? (self.view.frame.size.height - keyboardEndFrame.origin.y - 49 + 16) : 16;
        
        // Animate updating the view's layout by calling layoutIfNeeded inside a UIView animation block.
        [UIView animateWithDuration:animationDuration
                              delay:0
                            options:UIViewAnimationOptionCurveLinear // rawAnimationCurveValue
                         animations:^{
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
        
    }
}


#pragma mark - IBActions

- (IBAction)disconnectButtonTapped:(UIBarButtonItem *)sender {
    NSLog(@"TabViewController::disconnectButtonTapped");
    [[WebAPIConnection sharedConnection] disconnect];
}

- (IBAction)clearButtonTapped:(UIBarButtonItem *)sender {
    NSLog(@"TabViewController::clearButtonTapped");
    self.textView.text = @"";
}


#pragma mark - UITextViewDelegate

-(void) textViewDidBeginEditing:(UITextView *)textView {
    textView.text = @"";
    [self.view addGestureRecognizer:self.tapGR];
}

-(void) textViewDidEndEditing:(UITextView *)textView {
    [self.view removeGestureRecognizer:self.tapGR];
}

-(void) textViewDidChange:(UITextView *)textView {
    
}


@end
