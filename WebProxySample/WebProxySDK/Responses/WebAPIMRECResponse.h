//
//  WebAPIMRECResponse.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSJSONSerialization+Parse.h"

@interface WebAPIMRECResponse : NSObject
-(instancetype) init: (NSDictionary*) responseData;
-(NSDictionary*) getMRECResponse;
-(NSString*) JSONRepresentation;
-(NSArray*) getTranscription;
-(NSString*) getBestTranscription;
-(NSArray*) getConfidence;
@end
