//
//  WebAPIMRECResponse.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WebAPIMRECResponse.h"

@interface WebAPIMRECResponse()
@property (strong, nonatomic) NSDictionary* json;
@end

@implementation WebAPIMRECResponse


#pragma mark - Public Functions

-(instancetype) init: (NSDictionary*) responseData {
    self = [super init];
    if (self) {
        self.json = responseData;
    }
    return self;
}

-(NSDictionary*) getMRECResponse {
    return self.json;
}

-(NSString*) JSONRepresentation {
    if (self.json) {
        return [NSJSONSerialization stringify:self.json Pretty:true];
    } else {
        return nil;
    }
}

-(NSArray*) getTranscription {
    
    //v1.0
    //NSDictionary* user = self.json[@"results"][@"channels"][@"user"];
    //NSArray* transcript = [user objectForKey:@"transcript"];
    //NSDictionary* transcript0 = transcript[0];
    //NSString* text = [transcript0 objectForKey:@"text"];
    //NSArray* transcription = @[text];
    //if (transcription) {
    //    return transcription; //array of string.
    //}
    //return nil;
    
    //v1.1
    NSString* transcription = self.json[@"transcription"];
    if (transcription) {
        return @[self.json[@"transcription"]];
    } else {
        return nil;
    }
}

-(NSString*) getBestTranscription {
    NSArray * transcriptions = [self getTranscription];
    if (transcriptions && transcriptions.count > 0) {
        return transcriptions[0];
    }
    return nil;
}

-(NSArray*) getConfidence {
    NSArray* transcriptions = self.json[@"mrec_results"][@"confidences"];
    if (transcriptions) {
        return transcriptions;
    }
    return nil;
}

@end
