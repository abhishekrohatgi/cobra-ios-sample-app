//
//  WebAPIConnection.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebAPIStartSessionCommand.h"
#import "WebAPIEndSessionCommand.h"


@protocol WebAPIConnectionDelegate
-(void) webAPIConnectionDidConnect;
-(void) webAPIConnectionDidFail: (NSError*) error;
-(void) webAPIConnectionDidDisconnect: (NSString*) reason;
@end

@protocol WebAPISessionDelegate
-(void) webAPISessionDidStartSession: (NSString*) text;
-(void) webAPISessionDidFail: (NSString*) text;
-(void) webAPISessionDidEnd: (NSString*) text;
@end


@interface WebAPIConnection : NSObject <SRWebSocketDelegate, WebAPIStartSessionCommandDelegate,WebAPIEndSessionCommandDelegate>

typedef enum {
    WEBAPI_START_SESSION_CMD,
    WEBAPI_END_SESSION_CMD,
    WEBAPI_MREC_CMD,
    WEBAPI_TTS_CMD
} Command;
+ (NSString*) commandName: (Command) cmd;

+ (WebAPIConnection*) sharedConnection;

@property(strong, nonatomic) WebAPICommand * webAPICommand;
@property(weak, nonatomic) id <WebAPIConnectionDelegate> connectionDelegate;
@property(weak, nonatomic) id <WebAPISessionDelegate> sessionDelegate;
@property(nonatomic) BOOL loggingEnable; //default to true.
@property(strong, nonatomic) SRWebSocket * websocket;
@property(strong, nonatomic) NSMutableDictionary * defaultGrammarList;
@property(strong, nonatomic) NSString * sessionId;
-(NSMutableDictionary*) makeOptionalKeys;
-(void) connect:(NSURL*)uri;
-(void) disconnect;
-(void) startSession;
-(void) log : (NSString*)text;

@end


