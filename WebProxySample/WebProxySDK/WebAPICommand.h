//
//  WebAPICommand.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket+JSON.h"

@interface WebAPICommand : NSObject

@property (strong, nonatomic) NSString* name;

-(void) send; // Implement in subclass
-(void) handleMessage : (id) message; // Implement in subclass
-(void) sendCommand : (NSDictionary*) command;

@end
