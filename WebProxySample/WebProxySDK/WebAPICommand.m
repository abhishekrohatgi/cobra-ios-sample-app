//
//  WebAPICommand.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

//#import "WebAPICommand.h"
#import "WebAPIConnection.h"

@implementation WebAPICommand

-(void) send {
//    NSLog(@"WebAPICommand::send");
    [WebAPIConnection sharedConnection].webAPICommand = self;
    // Implement in subclass
}

-(void) handleMessage : (id) message {
//    NSLog(@"WebAPIConnection::handleMessage");
    // Implement in subclass
}

-(void) sendCommand : (NSDictionary*) command {
//    NSLog(@"WebAPICommand::sendCommand");
    NSMutableDictionary* json = [@{
                          @"type":@"command",
                          @"command": command
                          } mutableCopy];
    if([WebAPIConnection sharedConnection].sessionId != nil) {
        [json setObject:[WebAPIConnection sharedConnection].sessionId forKey:@"sessionId"];
    }
       
    [[WebAPIConnection sharedConnection].websocket sendJSON:json];
}

@end
