//
//  WAV.m
//  WebProxySample
//
//  Created by Nicolas on 2016-01-25.
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WAV.h"

@implementation WAV


+(NSData*) createPlayableFileFromPCMData: (NSData*) data {
    
    NSString* documentPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true)[0];
    NSString* filePath = [documentPath stringByAppendingString:@"/temp.wav"];
    
    [data writeToFile:filePath atomically:true];
    
    int numChannels             = 1;
    int bitsPerSample           = 16;
    int samplingRate            = 8000;
    unsigned long numSamples    = data.length;
    
    int byteRate                = numChannels * bitsPerSample * samplingRate / 8;
    int blockAlign              = numChannels * bitsPerSample / 8;
    unsigned long dataSize      = numChannels * numSamples * bitsPerSample / 8;
    int chunkSize               = 16;
    unsigned long totalSize     = 46 + dataSize;
    int audioFormat             = 1;
    
    FILE * fout = fopen([filePath cStringUsingEncoding:NSUTF8StringEncoding], "w");
    
    fwrite("RIFF", sizeof(int8_t), 4, fout);
    fwrite(&totalSize, sizeof(int32_t), 1, fout);
    fwrite("WAVE", sizeof(int8_t), 4, fout);
    fwrite("fmt ", sizeof(int8_t), 4, fout);
    fwrite(&chunkSize, sizeof(int32_t), 1, fout);
    fwrite(&audioFormat, sizeof(int16_t), 1, fout);
    fwrite(&numChannels, sizeof(int16_t), 1,fout);
    fwrite(&samplingRate, sizeof(int32_t), 1, fout);
    fwrite(&byteRate, sizeof(int32_t), 1, fout);
    fwrite(&blockAlign, sizeof(int16_t), 1, fout);
    fwrite(&bitsPerSample, sizeof(int16_t), 1, fout);
    fwrite("data", sizeof(int8_t), 4, fout);
    fwrite(&dataSize, sizeof(int32_t), 1, fout);
    
    fclose(fout);
    
    NSFileHandle * handle = [NSFileHandle fileHandleForUpdatingAtPath:filePath];
    
    [handle seekToEndOfFile];
    [handle writeData:data];
    [handle closeFile];
    
    return [[NSData alloc] initWithContentsOfFile:filePath];
}

@end
