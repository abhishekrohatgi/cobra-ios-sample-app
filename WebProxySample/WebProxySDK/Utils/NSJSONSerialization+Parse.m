//
//  NSJSONSerialization+Parse.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "NSJSONSerialization+Parse.h"

@implementation NSJSONSerialization (Parse)

+(NSString*) stringify: (NSObject*)object Pretty: (BOOL)pretty {
    NSError * error;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                        options:pretty ? NSJSONWritingPrettyPrinted : 0
                                                          error:&error];
    if (!jsonData) {
        NSLog(@"NSJSONSerialization::stringify - Error stringify: %@", error);
        return nil;
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

+(id) parse: (NSString*)string {
    NSError *error;
    id object = [NSJSONSerialization JSONObjectWithData:[NSMutableData dataWithData:[string dataUsingEncoding:NSUTF16StringEncoding]]
                                                options:NSJSONReadingMutableContainers
                                                  error:&error];
    if (!object) {
        NSLog(@"NSJSONSerialization::parseToObject - Error parsing JSON: %@", error);
        return nil;
    } else {
        return object;
    }
}

@end
