//
//  UIColor+MyColors.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <UIKit/UIKit.h>

@interface UIColor (MyColors)
+(UIColor *) aquaColor;
+(UIColor *) flatRedColor;
@end
