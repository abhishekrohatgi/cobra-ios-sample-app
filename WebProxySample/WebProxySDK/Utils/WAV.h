//
//  WAV.h
//  WebProxySample
//
//  Created by Nicolas on 2016-01-25.
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WAV : NSObject
+(NSData*) createPlayableFileFromPCMData: (NSData*) data;
@end
