//
//  UIColor+MyColors.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "UIColor+MyColors.h"

@implementation UIColor (MyColors)

+(UIColor *) aquaColor {
    return [[UIColor alloc] initWithRed:(0.0/255.0) green:(128.0/255.0) blue:(255.0/255.0) alpha:1.0];
}

+(UIColor *) flatRedColor {
    return [[UIColor alloc] initWithRed:(217.0/255.0) green:(56.0/255.0) blue:(41.0/255.0) alpha:1.0];
}



@end
