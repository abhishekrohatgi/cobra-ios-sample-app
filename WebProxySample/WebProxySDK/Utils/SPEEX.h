//
//  SPEEX.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "EZAudio.h"

@interface SPEEX : NSObject

+(BOOL) setupAudioSession;
+(AudioStreamBasicDescription) audioStreamBasicDescription;
+(int16_t*) encodeChannel: (float*)channel WithCount:(int)count;
+(int16_t) encodeSample: (float) sample;
+(float) decodeSample: (int16_t) sample;

@end
