//
//  NSJSONSerialization+Parse.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSJSONSerialization (Parse)

+(NSString*) stringify: (NSObject*)object Pretty: (BOOL)pretty;
+(id) parse: (NSString*)string;

@end
