//
//  SRWebSocket+JSON.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"
#import "NSJSONSerialization+Parse.h"

@interface SRWebSocket (JSON)
- (void)sendJSON:(NSDictionary*) json;
@end


