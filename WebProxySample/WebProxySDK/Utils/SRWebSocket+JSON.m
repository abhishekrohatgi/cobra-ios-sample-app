//
//  SRWebSocket+JSON.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

//#import "WebSocket.h"
#import "WebAPIConnection.h"

@implementation SRWebSocket (JSON)

-(void) sendJSON:(NSDictionary *)json {
//    NSLog(@"WebSocket::sendJSON");
    NSString *jsonString = [NSJSONSerialization stringify:json Pretty:true];
    [[WebAPIConnection sharedConnection] log:[NSString stringWithFormat:@"SENDING: %@", jsonString]];
    [self send:jsonString];
}

@end
