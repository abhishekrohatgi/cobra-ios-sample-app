//
//  SPEEX.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "SPEEX.h"

@implementation SPEEX

+(BOOL) setupAudioSession {
    NSLog(@"SPEEX::setupAudioSession");
    AVAudioSession* session = [AVAudioSession sharedInstance];
    NSError* err;
    
    AVAudioSessionCategoryOptions newOptions = session.categoryOptions | AVAudioSessionCategoryOptionDefaultToSpeaker;
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:newOptions error:&err];
    if (err) {
        NSLog(@"SPEEX - Error setting up audio session - setCategory");
        return false;
    }
    
    [session setPreferredInputNumberOfChannels:1 error:&err];
    if (err) {
        NSLog(@"SPEEX - Error setting up audio session - setPreferedInputNumberOfChannels");
        return false;
    }
    
    [session setPreferredSampleRate:8000 error:&err];
    if (err) {
        NSLog(@"SPEEX - Error setting up audio session - setPreferedSampleRate");
        return false;
    }
    
    [session setPreferredIOBufferDuration:0.5 error:&err];
    if (err) {
        NSLog(@"SPEEX - Error setting up audio session - setPreferedIOBufferDuration");
        return false;
    }
    
    [session setActive:true error:&err];
    if (err) {
        NSLog(@"SPEEX - Error setting up audio session - setActive");
        return false;
    }
    
    NSLog(@"  Sample Rate: %f", session.sampleRate);
    NSLog(@"  IO Buffer Duration: %f", session.IOBufferDuration);
    return true;
}

+(AudioStreamBasicDescription) audioStreamBasicDescription {
    NSLog(@"SPEEX::audioStreamBasicDescription");
    struct AudioStreamBasicDescription description;
    description.mSampleRate = 8000;
    description.mFormatID = kAudioFormatLinearPCM;
    description.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
    description.mBytesPerPacket = 2;
    description.mFramesPerPacket = 1;
    description.mBytesPerFrame = 2;
    description.mChannelsPerFrame = 1;
    description.mBitsPerChannel = 16;
    description.mReserved = 0;
    return description;
}

+(int16_t*) encodeChannel:(float *)channel WithCount:(int)count {
//    NSLog(@"SPEEX::encodeChannelWithCount - caller, DON'T FORGET TO free(encodeSpx);");
    int16_t* encodeSpx = malloc(sizeof(int16_t) * count);
    for (int i=0; i<count; i++) {
        encodeSpx[i] = [SPEEX encodeSample:channel[i]];
    }
    return encodeSpx;
}

+(int16_t) encodeSample:(float)sample {
//    NSLog(@"SPEEX::encodeSample");
    if (sample <= -1.0) {
        return INT16_MIN; // -32,768 (min)
    } else if (sample >= 1.0) {
        return INT16_MAX; // 32,767 (max)
    } else {
        return INT16_C(sample*INT16_MAX); // mid (percentage)
    }
}

+(float) decodeSample:(int16_t)sample {
    NSLog(@"SPEEX::decodeSample: return %f", (float)sample/(float)INT16_MAX);
    return (float)sample / (float)INT16_MAX;
}


@end
