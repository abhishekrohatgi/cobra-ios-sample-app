//
//  WebAPIConnection.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WebAPIConnection.h"

@interface WebAPIConnection ()

@property(nonatomic) BOOL connected; //default to False.
@property(strong, nonatomic) NSString * jsessionid;
@property(nonatomic) int requestId; //default to 0.

@end

@implementation WebAPIConnection


#pragma mark - Lazy instantiation & default values.

+ (WebAPIConnection *) sharedConnection {
    static WebAPIConnection * conn = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        conn = [[WebAPIConnection alloc] init];
    });
    return conn;
}
-(BOOL) loggingEnable {
    if (!_loggingEnable) _loggingEnable = true; //default id True.
    return _loggingEnable;
}
-(SRWebSocket*) websocket {
    if (!_websocket) _websocket = [[SRWebSocket alloc] init];
    return _websocket;
}
-(NSMutableDictionary*) defaultGrammarList {
    if (!_defaultGrammarList) _defaultGrammarList = [[NSMutableDictionary alloc] init];
    return _defaultGrammarList;
}
-(BOOL) connected {
    if (!_connected) _connected = false; //default is False.
    return _connected;
}
-(int) requestId {
    if (!_requestId) _requestId = 0; //default is 0.
    return _requestId;
}


#pragma mark - Public functions

+ (NSString*) commandName: (Command) cmd {
    NSString *result = nil;
    switch(cmd) {
        case WEBAPI_START_SESSION_CMD:
            result = @"startSession";
            break;
        case WEBAPI_END_SESSION_CMD:
            result = @"endSession";
            break;
        case WEBAPI_MREC_CMD:
            result = @"speechRecognition";
            break;
        case WEBAPI_TTS_CMD:
            result = @"speechSynthesis";
            break;
        default:
            result = @"unknown";
    }
    return result;
}

-(NSMutableDictionary*) makeOptionalKeys {
    NSMutableDictionary * keys = [[NSMutableDictionary alloc] init];
    [keys setObject:[[NSNumber numberWithInt:++self.requestId] stringValue] forKey:@"request_id"];
    [keys setObject:@"js" forKey:@"phone_OS"];
    
    if (self.jsessionid) {
        [keys setObject:self.jsessionid forKey:@"jsessionid"];
    }
    return keys;
}

-(void) connect:(NSURL*)uri {
    [self log:@"WebAPIConnection::connect"];
    if (self.connected) return;
    
    self.websocket = [self.websocket initWithURL:uri];
    self.websocket.delegate = self;
    [self.websocket open];
}

-(void) disconnect {
    [self log:@"WebAPIConnection::disconnect"];
    if (self.connected) {
        WebAPIEndSessionCommand* command = [[WebAPIEndSessionCommand alloc] init];
        command.delegate = self;
        [command send];
    }
}

-(void) startSession {
    [self log:@"WebAPIConnection::startSession"];
    WebAPIStartSessionCommand* command = [[WebAPIStartSessionCommand alloc] initWithAgent:[[NSUserDefaults standardUserDefaults] stringForKey:@"niw_url"]];
    command.delegate = self;
    [command send];
}

-(void) log : (NSString*)text {
    if (self.loggingEnable) {
        NSLog(@"%@", text);
    }
}


#pragma mark - Private functions

-(void) reset {
    [self log:@"WebAPIConnection::reset"];
    self.websocket = nil;
    self.defaultGrammarList = nil;
    self.connected = false;
    self.requestId = 0;
    self.webAPICommand = nil;
}


#pragma mark - SRWebSocketDelegate

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(NSObject*)message {
    [self log: [NSString stringWithFormat:@"WebAPIConnection::webSocketDidReceiveMessage RECEIVED: %@", message]];
    if (self.webAPICommand) {
        [self.webAPICommand handleMessage:message];
    }
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    [self log:[NSString stringWithFormat:@"WebAPIConnection::webSocketDidOpen"]];
    if (!self.connected) {
        self.connected = true;
        [self.connectionDelegate webAPIConnectionDidConnect];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [self log:[NSString stringWithFormat:@"WebAPIConnection::webSocket didFailWithError error: %@", error]];
    [self.connectionDelegate webAPIConnectionDidFail:error];
    [self reset];
}

- (void) webSocket: (SRWebSocket *)webSocket
  didCloseWithCode:(NSInteger)code
            reason:(NSString *)reason
          wasClean:(BOOL)wasClean {
    [self log:[NSString stringWithFormat:@"WebAPIConnection::webSocket didCloseWithCode code: %ld, reason: %@, wasClean: %d", (long)code, reason, wasClean]];
    [self.connectionDelegate webAPIConnectionDidDisconnect:reason];
    [self reset];
}


#pragma mark - WebAPIStartSessionCommandDelegate

-(void) webAPIStartSessionCommandDidSucceed:(NSString *)text {
    NSLog(@"WebAPIConnection::webAPIStartSessionCommandDidSucceed");
    [self.sessionDelegate webAPISessionDidStartSession:text];
}

-(void) webAPIStartSessionCommandDidFail:(NSString *)text {
    NSLog(@"WebAPIConnection::webAPIStartSessionCommandDidFail");
    [self.sessionDelegate webAPISessionDidFail:text];
}


#pragma mark - WebAPIEndSessionCommandDelegate

-(void) webAPIEndSessionCommandDidSucceed:(NSString *)text {
    NSLog(@"WebAPIConnection::webAPIEndSessionCommandDidSucceed");
    [self.sessionDelegate webAPISessionDidEnd:text];
    if (self.websocket) {
        [self.websocket close];
    }
    [self reset];
}

-(void) webAPIEndSessionCommandDidFail:(NSString *)text {
    NSLog(@"WebAPIConnection::webAPIEndSessionCommandDidFail");
    [self.sessionDelegate webAPISessionDidFail:text];
}


@end
