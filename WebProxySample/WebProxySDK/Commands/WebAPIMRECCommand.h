//
//  WebAPIMRECCommand.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WebAPIConnection.h"
#import "WebAPIMRECResponse.h"
#import "SPEEX.h"

@protocol WebAPIMRECCommandDelegate
-(void) webAPIMRECCommandDidReceiveIntermediateResult: (NSString*) response;
-(void) webAPIMRECCommandDidSucceed: (NSString*) response;
-(void) webAPIMRECCommandDidFail: (NSString*) text;
-(void) webAPIMRECCommandDidReceiveStartOfSpeech: (NSString*) text;
-(void) webAPIMRECCommandDidReceiveEndOfSpeech: (NSString*) text;
@end

@interface WebAPIMRECCommand : WebAPICommand <EZMicrophoneDelegate>

@property (nonatomic) BOOL sendIntermediateResponses; //default is true.
@property (strong, nonatomic) id<WebAPIMRECCommandDelegate> delegate;

-(BOOL) startRecording;
-(BOOL) stopRecording;
-(BOOL) isRecording;
-(BOOL) isDoneRecording;

@end
