//
//  WebAPIEndSessionCommand.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

//#import "WebAPIEndSessionCommand.h"
#import "WebAPIConnection.h"

@implementation WebAPIEndSessionCommand

-(instancetype) init {
    self = [super init];
    if (self) {
        self.name = [WebAPIConnection commandName:WEBAPI_END_SESSION_CMD];
    }
    return self;
}


-(void) send {
    NSLog(@"WebAPIEndSessionCommand::send");
    [super send];
    [self sendCommand:@{
                        @"name":self.name,
                        @"id":@"lastCommand"
                        }];
}

-(void) handleMessage: (id) message {
    NSLog(@"WebAPIEndSessionCommand::handleMessage");
    NSDictionary* json = [NSJSONSerialization parse:(NSString*)message];
    if (json){
        NSString* response = [NSJSONSerialization stringify:json Pretty:true];
        if ([[json allKeys] containsObject:@"result"] &&
            [json[@"result"][@"status"] isEqualToString:@"success"]) {
            [self.delegate webAPIEndSessionCommandDidSucceed:response];
            [WebAPIConnection sharedConnection].sessionId = nil;
        } else if ([[json allKeys] containsObject:@"errors"]) {
            [self.delegate webAPIEndSessionCommandDidFail:response];
        } else {
            NSLog(@"  WEIRD message: %@", json);
            [self.delegate webAPIEndSessionCommandDidFail:[NSString stringWithFormat:@"Invalid message received: %@", response]];
        }
    }
}

@end
