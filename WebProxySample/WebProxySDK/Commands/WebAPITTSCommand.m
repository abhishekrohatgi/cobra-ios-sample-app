//
//  WebAPITTSCommand.m
//  WebProxySample
//
//  Created by Nicolas on 2016-01-25.
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WebAPITTSCommand.h"

@implementation WebAPITTSCommand

#pragma mark - Lazy instantiation & default values.

-(instancetype) init {
    self = [super init];
    if (self) {
        self.name = [WebAPIConnection commandName:WEBAPI_TTS_CMD];
    }
    return self;
}

-(instancetype) init:(NSString *)input {
    self = [self init];
    if (self) {
        self.input = input;
    }
    return self;
}


#pragma mark - Public Functions

-(void) send {
    NSLog(@"WebAPITTSCommand::send");
    if (self.input) {
        [super send];
        NSDictionary* command = @{@"name":self.name,
                                  @"id":@"1",
                                  @"parameters":@{
                                          @"data":self.input,
                                          @"type":@"text",
                                          }
                                  };
        [self sendCommand:command];
    }
}

-(void) handleMessage:(id) message {
    NSLog(@"WebAPITTSCommand::handleMessage");
    if ([message isKindOfClass:[NSData class]]) {
        [self.delegate webAPITTSCommandDidReceiveAudio:[WAV createPlayableFileFromPCMData:(NSData*)message]];
    }
    else if([message isKindOfClass:[NSString class]]) {
        NSDictionary* json = [NSJSONSerialization parse:(NSString*)message];
        if([[json allKeys] containsObject:@"result"] &&
           [json[@"result"][@"status"] isEqualToString:@"success"]) {
            [self.delegate webAPITTSCommandDidFinish:message];
        } else {
            [self.delegate webAPITTSCommandDidFail:message];
        }
    } else {
        NSLog(@"  UNKNOWN message: %@", message);
        [self.delegate webAPITTSCommandDidFail:[NSString stringWithFormat:@"Invalid message received: %@", message]];
    }
}

@end
