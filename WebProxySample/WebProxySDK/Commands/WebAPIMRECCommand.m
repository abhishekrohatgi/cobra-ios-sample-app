//
//  WebAPIMRECCommand.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WebAPIMRECCommand.h"

@interface WebAPIMRECCommand ()

@property (strong, nonatomic) EZMicrophone* microphone;
@property (nonatomic) BOOL doneRecording; //default is false.

@end

@implementation WebAPIMRECCommand


#pragma mark - Lazy instantiation & default values.

-(instancetype) init {
    self = [super init];
    if (self) {
        self.name = [WebAPIConnection commandName:WEBAPI_MREC_CMD];
        [SPEEX setupAudioSession];
        self.microphone = [[EZMicrophone alloc] initWithMicrophoneDelegate:self
                                           withAudioStreamBasicDescription:[SPEEX audioStreamBasicDescription]];
        self.doneRecording = false;
        self.sendIntermediateResponses = true;
    }
    return self;
}


#pragma mark - Public Functions

// No send() method, use startRecording() instead for this command.

-(void) handleMessage:(id) message {
    
    // get responses from Recording ...
    
    NSLog(@"WebAPIMRECCommand::handleMessage");
    NSDictionary* json = [NSJSONSerialization parse:(NSString*)message];
    if (json){
        
        NSString* response = [NSJSONSerialization stringify:json Pretty:true];
        if ([[json allKeys] containsObject:@"result"] &&
                   [json[@"result"][@"status"] isEqualToString:@"success"]) {
            NSNumber* final_response = json[@"result"][@"final"];
            if ( final_response && (final_response.boolValue == YES) ) {
                [self.delegate webAPIMRECCommandDidSucceed:message];
                [self stopRecording];
            } else {
                [self.delegate webAPIMRECCommandDidReceiveIntermediateResult:message];
            }
        } else if ([[json allKeys] containsObject:@"event"]) {
            if([json[@"event"][@"name"] isEqualToString:@"startOfSpeech"]) {
                [self.delegate webAPIMRECCommandDidReceiveStartOfSpeech:message];
            }
            else if([json[@"event"][@"name"] isEqualToString:@"endOfSpeech"]) {
                [self.delegate webAPIMRECCommandDidReceiveEndOfSpeech:message];
            }
        } else if ([[json allKeys] containsObject:@"errors"]) {
            [self.delegate webAPIMRECCommandDidFail:response];
        } else {
            NSLog(@"  WEIRD message: %@", json);
            [self.delegate webAPIMRECCommandDidFail:[NSString stringWithFormat:@"Invalid message received: %@", response]];
        }
    }
}

-(BOOL) startRecording {
    NSLog(@"WebAPIMRECCommand::startRecording");
    if (!self.doneRecording && !self.microphone.microphoneOn) {
        [super send];
        [self sendCommand:@{
                            @"name":self.name,
                            @"id":@"1",
                            @"parameters":@{
                                    @"language": @"en-us",
                                    @"endPointDetection": @YES,
                                    @"voiceThreshold": @2,
                                    @"stopOnEndOfSpeech": @YES,
                                    @"wordStream": @YES
                                }
                            }];
        self.microphone.microphoneOn = true;
        return true;
    }
    return false;
}


-(BOOL) stopRecording {
    NSLog(@"WebAPIMRECCommand::stopRecording");
    if (self.microphone.microphoneOn) {
        self.microphone.microphoneOn = false;
        self.doneRecording = true;
        return true;
    } else {
        return false;
    }
}


-(BOOL) isRecording {
    return self.microphone.microphoneOn;
}

-(BOOL) isDoneRecording {
    return self.doneRecording;
}


#pragma mark - EZMicrophoneDelegate

-(void) microphone:(EZMicrophone *)microphone changedDevice:(EZAudioDevice *)device {
    [[WebAPIConnection sharedConnection] log:[NSString stringWithFormat:@"WebAPIMRECCommand::microphone changedDevice device: %@", device]];
}

-(void) microphone:(EZMicrophone *)microphone changedPlayingState:(BOOL)isPlaying {
    [[WebAPIConnection sharedConnection] log:[NSString stringWithFormat:@"WebAPIMRECCommand::microphone changedPlayingState isPlaying: %d", isPlaying]];
}

- (void) microphone :(EZMicrophone *)microphone
    hasAudioReceived:(float **)buffer
      withBufferSize:(UInt32)bufferSize
withNumberOfChannels:(UInt32)numberOfChannels {
    [[WebAPIConnection sharedConnection] log:[NSString stringWithFormat:@"WebAPIMRECCommand::microphone hasAudioReceived bufferSize: %d numberOfChannels: %d", (unsigned int)bufferSize, (unsigned int)numberOfChannels]];
    int16_t* encodedSpeex = [SPEEX encodeChannel:buffer[0] WithCount:(int)bufferSize];
    NSData* data = [[NSData alloc] initWithBytes:encodedSpeex length:(int)bufferSize*sizeof(int16_t)];
    free(encodedSpeex);
    [[WebAPIConnection sharedConnection].websocket send:data];
}

-(void) microphone:(EZMicrophone *)microphone hasAudioStreamBasicDescription:(AudioStreamBasicDescription)audioStreamBasicDescription {
    [[WebAPIConnection sharedConnection] log:[NSString stringWithFormat:@"WebAPIMRECCommand::microphone hasAudioStreamBasicDescription: %@", [EZAudioUtilities stringForAudioStreamBasicDescription:audioStreamBasicDescription]]];
}

@end
