//
//  WebAPIEndSessionCommand.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WebAPICommand.h"

@protocol WebAPIEndSessionCommandDelegate
-(void) webAPIEndSessionCommandDidSucceed: (NSString*) text;
-(void) webAPIEndSessionCommandDidFail: (NSString*)text;
@end

@interface WebAPIEndSessionCommand : WebAPICommand
@property (strong, nonatomic) id<WebAPIEndSessionCommandDelegate> delegate;
@end
