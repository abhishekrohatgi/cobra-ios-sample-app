//
//  WebAPIStartSessionCommand.m
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

//#import "WebAPIStartSessionCommand.h"
#import "WebAPIConnection.h"

@interface WebAPIStartSessionCommand()
@property(nonatomic, strong) NSString* agent;
@end

@implementation WebAPIStartSessionCommand

-(instancetype) initWithAgent: (NSString*) url {
    self = [super init];
    if (self) {
        self.name = [WebAPIConnection commandName:WEBAPI_START_SESSION_CMD];
        self.agent = url;
    }
    return self;
}

-(void) send {
    NSLog(@"WebAPIStartSessionCommand::send");
    [super send];
        
    NSDictionary* command = @{@"name":self.name,
                              @"id":@"1",
                              @"parameters":@{
                                      @"applicationKey":[[NSUserDefaults standardUserDefaults] objectForKey:@"applicationKey"],
                                      @"verificationCode":[[NSUserDefaults standardUserDefaults] objectForKey:@"verificationCode"],
                                      }
                              };
    [self sendCommand:command];
}

-(void) handleMessage: (id) message {
    NSLog(@"WebAPIStartSessionCommand::handleMessage");
    NSDictionary* json = [NSJSONSerialization parse:(NSString*)message];
    if (json){
        NSString* response = [NSJSONSerialization stringify:json Pretty:true];
        if ([[json allKeys] containsObject:@"result"] &&
            [json[@"result"][@"status"] isEqualToString:@"success"]) {
            [WebAPIConnection sharedConnection].sessionId = json[@"sessionId"];
            [self.delegate webAPIStartSessionCommandDidSucceed:response];
        } else if ([[json allKeys] containsObject:@"errors"]) {
            [self.delegate webAPIStartSessionCommandDidFail:response];
        } else {
            NSLog(@"  WEIRD message: %@", json);
            [self.delegate webAPIStartSessionCommandDidFail:[NSString stringWithFormat:@"Invalid message received: %@", response]];
        }
    }
}

@end
