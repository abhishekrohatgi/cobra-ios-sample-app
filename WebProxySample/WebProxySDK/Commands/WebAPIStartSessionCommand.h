//
//  WebAPIStartSessionCommand.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebAPICommand.h"

@protocol WebAPIStartSessionCommandDelegate
-(void) webAPIStartSessionCommandDidSucceed: (NSString*) text;
-(void) webAPIStartSessionCommandDidFail: (NSString*) text;
@end

@interface WebAPIStartSessionCommand : WebAPICommand
@property (strong, nonatomic) id <WebAPIStartSessionCommandDelegate> delegate;
-(instancetype) initWithAgent: (NSString*) url;
@end
