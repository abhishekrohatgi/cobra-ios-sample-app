//
//  WebAPITTSCommand.h
//  WebProxySample
//
//  Created by Nicolas on 2016-01-25.
//  Copyright © 2017 Nuance. All rights reserved.
//

#import "WebAPIConnection.h"
#import "WAV.h"

@protocol WebAPITTSCommandDelegate
-(void) webAPITTSCommandDidReceiveAudio: (NSData*) data;
-(void) webAPITTSCommandDidFinish: (NSString*) text;
-(void) webAPITTSCommandDidFail: (NSString*) text;
@end

@interface WebAPITTSCommand : WebAPICommand
@property(strong, nonatomic) NSString* input;
@property(strong, nonatomic) NSString* type; //default is "text".
@property(strong, nonatomic) NSString* voice; //default is "Carol".
@property(strong, nonatomic) NSString* language; //default is "en-us".
@property(strong, nonatomic) id<WebAPITTSCommandDelegate> delegate;

-(instancetype)init : (NSString*) input;
@end
