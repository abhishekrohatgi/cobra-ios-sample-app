//
//  AppDelegate.h
//  WebProxySample
//
//  Copyright © 2017 Nuance. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

